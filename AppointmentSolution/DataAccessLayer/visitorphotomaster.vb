﻿'------------------------------------------------------------
'created for table [visitorphotomaster]  on 25-Mar-2018
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class visitorphotomaster
    Inherits UneeshHelper
    Public ID As Integer
    Public FileName As String
    Public FilePath As String
    Public VisitorID As Integer
    Public FileType As Integer
    Public IpAddress As String
    Public Status As Byte
    Public UploadedDate As DateTime

    Public Sub New(Optional ByVal visitorphotomasterID As Integer = 0)
        MyBase.New(visitorphotomasterID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertvisitorphotomaster"
        IUDProcedures.UpdateProcedure = "Updatevisitorphotomaster"
        IUDProcedures.DeleteProcedure = "Deletevisitorphotomaster"
    End Sub
    Protected Overrides Sub getData()
        If ObjId = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from visitorphotomaster where ID=" & ObjId, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            FileName = IsNullValue(dr("FileName"), "")
            FilePath = IsNullValue(dr("FilePath"), "")
            VisitorID = IsNullValue(dr("VisitorID"), 0)
            FileType = IsNullValue(dr("FileType"), 0)
            IpAddress = IsNullValue(dr("IpAddress"), "")
            Status = IsNullValue(dr("Status"), 0)
            UploadedDate = IsNullValue(dr("UploadedDate"), New Date(0))
        Else
            ObjId = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = {
        New MySqlParameter("@ID", MySqlDbType.Int16),
        New MySqlParameter("@FileName", MySqlDbType.String, 500),
        New MySqlParameter("@FilePath", MySqlDbType.String, 500),
        New MySqlParameter("@VisitorID", MySqlDbType.Int16),
        New MySqlParameter("@FileType", MySqlDbType.Int16),
        New MySqlParameter("@IpAddress", MySqlDbType.String, 50),
        New MySqlParameter("@Status", MySqlDbType.Byte),
        New MySqlParameter("@UploadedDate", MySqlDbType.DateTime)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(FileName, DBNull.Value)
        sp_params(2).Value = IsNullValue(FilePath, DBNull.Value)
        sp_params(3).Value = IsNullValue(VisitorID, 0)
        sp_params(4).Value = IsNullValue(FileType, 0)
        sp_params(5).Value = IsNullValue(IpAddress, DBNull.Value)
        sp_params(6).Value = IsNullValue(Status, 0)
        sp_params(7).Value = dbDate(UploadedDate)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input

        Return sp_params
    End Function
End Class