﻿Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Web
Imports MySql.Data.MySqlClient

Public Class Tools
    Public Shared Sub ExportToExcel(ByVal data As MySqlDataReader, ByVal reportName As String, ByVal filters As String)
        Dim i As Integer
        If data.HasRows Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Charset = ""
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"

            HttpContext.Current.Response.Write("<table border='1' class='printpreview'>")
            If Not String.IsNullOrEmpty(reportName) Then
                HttpContext.Current.Response.Write("<tr><td colspan=" & data.FieldCount & ">" & reportName & "</tr>")
            End If

            If Not String.IsNullOrEmpty(filters) Then
                HttpContext.Current.Response.Write("<tr><td colspan=" & data.FieldCount & ">" & filters & "</tr>")
            End If

            HttpContext.Current.Response.Write("<tr>")
            For i = 0 To data.FieldCount - 1
                HttpContext.Current.Response.Write("<td style='background-color:#D5EDB3font-weight:bold'>" & data.GetName(i).ToString() & "</td>")
            Next
            HttpContext.Current.Response.Write("</tr>")
            While data.Read
                HttpContext.Current.Response.Write("<tr>")
                For i = 0 To data.FieldCount - 1
                    HttpContext.Current.Response.Write("<td>" & data.GetValue(i).ToString() & "</td>")
                Next
                HttpContext.Current.Response.Write("</tr>")
            End While
            HttpContext.Current.Response.Write("</table>")
            HttpContext.Current.Response.End()
        End If
        data.Close()
    End Sub
    Public Shared Sub ExportToCSV(ByVal data As DataSet, ByVal reportName As String, ByVal filters As String)
        Dim i, j As Integer
        Dim str As New StringBuilder

        If data.Tables(0).Rows.Count > 0 Then
            For i = 0 To data.Tables(0).Columns.Count - 1
                If str.Length = 0 Then
                    str.Append(data.Tables(0).Columns(i).ColumnName.ToString)
                Else
                    str.Append(",")
                    str.Append(data.Tables(0).Columns(i).ColumnName.ToString)
                End If
            Next
            str.AppendLine()
            For i = 0 To data.Tables(0).Rows.Count - 1
                For j = 0 To data.Tables(0).Columns.Count - 1
                    str.Append(data.Tables(0).Rows(i)(j).ToString())
                    str.Append(",")
                Next
                str.AppendLine()
            Next
        End If

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & reportName & ".csv")
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.csv"
        Response.Write(str.ToString())
        Response.End()
    End Sub
    'Public Shared Sub ExportToExcel(ByVal data As DataTable, ByVal reportName As String, ByVal filters As String)
    '    Dim i As Integer
    '    If data.Rows.Count > 1 Then
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.Charset = ""
    '        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"

    '        HttpContext.Current.Response.Write("<table border='1' class='printpreview'>")
    '        If Not String.IsNullOrEmpty(reportName) Then
    '            HttpContext.Current.Response.Write("<tr><td colspan=" & data.Rows.Count & ">" & reportName & "</tr>")
    '        End If

    '        If Not String.IsNullOrEmpty(filters) Then
    '            HttpContext.Current.Response.Write("<tr><td colspan=" & data.Rows.Count & ">" & filters & "</tr>")
    '        End If

    '        HttpContext.Current.Response.Write("<tr>")
    '        For i = 0 To data.Rows.Count - 1
    '            HttpContext.Current.Response.Write("<td style='background-color:#D5EDB3font-weight:bold'>" & data.GetName(i).ToString() & "</td>")
    '        Next
    '        HttpContext.Current.Response.Write("</tr>")
    '        While data.Read
    '            HttpContext.Current.Response.Write("<tr>")
    '            For i = 0 To data.FieldCount - 1
    '                HttpContext.Current.Response.Write("<td>" & data.GetValue(i).ToString() & "</td>")
    '            Next
    '            HttpContext.Current.Response.Write("</tr>")
    '        End While
    '        HttpContext.Current.Response.Write("</table>")
    '        HttpContext.Current.Response.End()
    '    End If
    '    data.Close()
    'End Sub
End Class
