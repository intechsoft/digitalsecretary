﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Appointment.Master" CodeBehind="VisitorRegistor.aspx.vb" Inherits="AppointmentSolution.VisitorRegistor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/Webcam_Plugin/jquery.webcam.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contact-us-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="contact-us-label" aria-hidden="true" style="top: 132.4px; position: absolute; display: block;">
        <div class="modal-header" style="border-bottom: 3px solid #fff;">
            <!-- ## heading ## -->
            <h5 style="float: right;">
                <%=Now.ToString("hh:mm:ss tt") %></h5>
            <div style="">
                <h3 id="contact-us-label" style="margin: 0 0 0 0; padding: 0 0 0 0;">Visitor</h3>
            </div>

        </div>
        <h4></h4>
        <div class="modal-body">
            <!-- ## content ## -->
            <div class="row-fluid">
                <div id="contact_form" class="span6 offset1">
                    <div class="controls">
                        <label class="control-label" for="txtFirstName">First Name</label>
                        <asp:TextBox ID="txtFirstName" ClientIDMode="Static" runat="server" placeholder="First Name" class="span10 txt required"></asp:TextBox>
                    </div>
                    <div class="controls">
                        <label class="control-label" for="txtSecondName">Second Name</label>
                        <asp:TextBox ID="txtSecondName" ClientIDMode="Static" runat="server" placeholder="Second Name" class="span10 txt required"></asp:TextBox>
                    </div>
                    <div class="controls">
                        <label class="control-label" for="txtCompany">Company</label>
                        <asp:TextBox runat="server" ClientIDMode="Static" placeholder="Company" class="span10 txt required" ID="txtCompany"></asp:TextBox>
                    </div>
                    <div class="controls">
                        <label class="control-label" for="txtMail">Mail</label>
                        <asp:TextBox runat="server" ClientIDMode="Static" placeholder="Mail" class="span10 txt required email" ID="txtMail"></asp:TextBox>
                    </div>
                    <div class="controls">
                        <label class="control-label" for="txtMobileNumber">Mobile</label>
                        <asp:TextBox runat="server" ClientIDMode="Static" placeholder="Mobile" class="span10 txt required" ID="txtMobileNumber"></asp:TextBox>
                    </div>
                    <div class="controls">
                        <asp:Button ID="btn_Send" runat="server" Text="Next" class="btn btn-submit right" Style="float: right;" OnClientClick="return validate();" />
                        <asp:Label ID="contact_form_message" runat="server" Text="" class="span8"></asp:Label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <div id="webcam">
                    </div>
                    <%-- <br />
                        <asp:Button ID="btnCapture" Text="Capture" runat="server" OnClientClick="return Capture();" />
                        <br />
                        <span id="camStatus"></span>--%>
                </div>
                <div class="clearfix">
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        var pageUrl = 'VisitorRegistor.aspx';
        $(function () {
            jQuery("#webcam").webcam({
                width: 320,
                height: 240,
                mode: "save",
                swffile: '<%=ResolveUrl("assets/Webcam_Plugin/jscam.swf") %>',
                debug: function (type, status) {
                    //$('#camStatus').append(type + ": " + status + '<br /><br />');
                },
                onSave: function (data) {
                    $.ajax({
                        type: "POST",
                        url: pageUrl + "/GetCapturedImage",
                        data: '',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            //$("[id*=imgCapture]").css("visibility", "visible");
                            //$("[id*=imgCapture]").attr("src", r.d);
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                },
                onCapture: function () {
                    webcam.save(pageUrl);
                }
            });
        });
        function Capture() {
            webcam.capture();
            return false;
        }

        function validate() {
            if (($("#txtFirstName").val()).trim().length > 0) {
                if ($("#txtSecondName").val().trim().length > 0) {
                    if ($("#txtCompany").val().trim().length > 0) {
                        if ($("#txtMail").val().trim().length > 0) {
                            if ($("#txtMobileNumber").val().trim().length > 0) {
                                return true;
                            } else {
                                alert("Mobile number cannot be left blank!!!");
                                $("#txtMobileNumber").focus();
                            }
                        } else {
                            alert("E-mail cannot be left blank!!!");
                            $("#txtMail").focus();
                        }
                    } else {
                        alert("Company name cannot be left blank!!!");
                        $("#txtCompany").focus();
                    }
                } else {
                    alert("Second name cannot be left blank!!!");
                    $("#txtSecondName").focus();
                }
            } else {
                alert("First name cannot be left blank!!!");
                $("#txtFirstName").focus();
            }
            return false;
        }

        $("#txtFirstName").focus();

    </script>
</asp:Content>
