﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Appointment.Master" CodeBehind="VisitorReceipt.aspx.vb" Inherits="AppointmentSolution.VisitorReceipt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="contact-us-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="contact-us-label" aria-hidden="true" style="top: 132.4px; position: absolute; display: block;">
        <div class="modal-header" style="border-bottom:3px solid #fff;">
            <!-- ## heading ## -->
            <h5 style="float:right;">
                <%=Now.ToString("hh:mm:ss tt") %></h5>
            <div style=""><h3 id="contact-us-label" style="margin:0 0 0 0; padding: 0 0 0 0;">Instructions</h3></div>
            
        </div>
            <h4></h4>
        <div class="modal-body">
            <!-- ## content ## -->
            <div class="row-fluid">
                <div id="contact_form" class="span6 offset1">
                            <h4 class="control-label">Please wait in the lobby.<br />
                                Someone will pick you up.<br />
                                This might take a few minutes.
                            </h4>
                    <div class="clearfix"></div>
                </div>
                
            </div>
        </div>
    </div>
</asp:Content>
