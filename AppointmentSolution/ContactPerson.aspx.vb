﻿Public Class ContactPerson
    Inherits BaseUIPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btn_Send_Click(sender As Object, e As EventArgs) Handles btn_Send.Click
        sendEmailWithUserDetailsToContactPerson(hidContactPerson.Value)
    End Sub

    Private Sub btnHRContactPerson1_Click(sender As Object, e As EventArgs) Handles btnHRContactPerson1.Click
        sendEmailWithUserDetailsToContactPerson("HR Contact Person 1")
    End Sub

    Private Sub btnHRContactPerson2_Click(sender As Object, e As EventArgs) Handles btnHRContactPerson2.Click
        sendEmailWithUserDetailsToContactPerson("HR Contact Person 2")
    End Sub

    Private Sub btnHRContactPerson3_Click(sender As Object, e As EventArgs) Handles btnHRContactPerson3.Click
        sendEmailWithUserDetailsToContactPerson("HR Contact Person 3")
    End Sub

    Private Sub btnHRContactPerson4_Click(sender As Object, e As EventArgs) Handles btnHRContactPerson4.Click
        sendEmailWithUserDetailsToContactPerson("HR Contact Person 4")
    End Sub

    Private Sub btnHRContactPerson5_Click(sender As Object, e As EventArgs) Handles btnHRContactPerson5.Click
        sendEmailWithUserDetailsToContactPerson("HR Contact Person 5")
    End Sub

    Private Sub btnITContactPerson1_Click(sender As Object, e As EventArgs) Handles btnITContactPerson1.Click
        sendEmailWithUserDetailsToContactPerson("IT Contact Person 1")
    End Sub

    Private Sub btnITContactPerson2_Click(sender As Object, e As EventArgs) Handles btnITContactPerson2.Click
        sendEmailWithUserDetailsToContactPerson("IT Contact Person 2")
    End Sub

    Private Sub btnITContactPerson3_Click(sender As Object, e As EventArgs) Handles btnITContactPerson3.Click
        sendEmailWithUserDetailsToContactPerson("IT Contact Person 3")
    End Sub

    Private Sub btnITContactPerson4_Click(sender As Object, e As EventArgs) Handles btnITContactPerson4.Click
        sendEmailWithUserDetailsToContactPerson("IT Contact Person 4")
    End Sub

    Private Sub btnITContactPerson5_Click(sender As Object, e As EventArgs) Handles btnITContactPerson5.Click
        sendEmailWithUserDetailsToContactPerson("IT Contact Person 5")
    End Sub

    Private Sub btnMarketingContactPerson1_Click(sender As Object, e As EventArgs) Handles btnMarketingContactPerson1.Click
        sendEmailWithUserDetailsToContactPerson("Marketing Contact Person 1")
    End Sub

    Private Sub btnMarketingContactPerson2_Click(sender As Object, e As EventArgs) Handles btnMarketingContactPerson2.Click
        sendEmailWithUserDetailsToContactPerson("Marketing Contact Person 2")
    End Sub

    Private Sub btnMarketingContactPerson3_Click(sender As Object, e As EventArgs) Handles btnMarketingContactPerson3.Click
        sendEmailWithUserDetailsToContactPerson("Marketing Contact Person 3")
    End Sub

    Private Sub btnMarketingContactPerson4_Click(sender As Object, e As EventArgs) Handles btnMarketingContactPerson4.Click
        sendEmailWithUserDetailsToContactPerson("Marketing Contact Person 4")
    End Sub

    Private Sub btnMarketingContactPerson5_Click(sender As Object, e As EventArgs) Handles btnMarketingContactPerson5.Click
        sendEmailWithUserDetailsToContactPerson("Marketing Contact Person 5")
    End Sub

    Private Sub btnProductionContactPerson1_Click(sender As Object, e As EventArgs) Handles btnProductionContactPerson1.Click
        sendEmailWithUserDetailsToContactPerson("Production Contact Person 1")
    End Sub

    Private Sub btnProductionContactPerson2_Click(sender As Object, e As EventArgs) Handles btnProductionContactPerson2.Click
        sendEmailWithUserDetailsToContactPerson("Production Contact Person 2")
    End Sub

    Private Sub btnProductionContactPerson3_Click(sender As Object, e As EventArgs) Handles btnProductionContactPerson3.Click
        sendEmailWithUserDetailsToContactPerson("Production Contact Person 3")
    End Sub

    Private Sub btnProductionContactPerson4_Click(sender As Object, e As EventArgs) Handles btnProductionContactPerson4.Click
        sendEmailWithUserDetailsToContactPerson("Production Contact Person 4")
    End Sub

    Private Sub btnProductionContactPerson5_Click(sender As Object, e As EventArgs) Handles btnProductionContactPerson5.Click
        sendEmailWithUserDetailsToContactPerson("Production Contact Person 5")
    End Sub

    Private Sub btnReceptionContactPerson1_Click(sender As Object, e As EventArgs) Handles btnReceptionContactPerson1.Click
        sendEmailWithUserDetailsToContactPerson("Reception Contact Person 5")
    End Sub

    Private Sub btnReceptionContactPerson2_Click(sender As Object, e As EventArgs) Handles btnReceptionContactPerson2.Click
        sendEmailWithUserDetailsToContactPerson("Reception Contact Person 5")
    End Sub

    Private Sub btnReceptionContactPerson3_Click(sender As Object, e As EventArgs) Handles btnReceptionContactPerson3.Click

    End Sub

    Private Sub btnReceptionContactPerson4_Click(sender As Object, e As EventArgs) Handles btnReceptionContactPerson4.Click
        sendEmailWithUserDetailsToContactPerson("Reception Contact Person 5")
    End Sub

    Private Sub btnReceptionContactPerson5_Click(sender As Object, e As EventArgs) Handles btnReceptionContactPerson5.Click
        sendEmailWithUserDetailsToContactPerson("Reception Contact Person 5")
    End Sub

    Private Sub sendEmailWithUserDetailsToContactPerson(contactPerson As String)
        Dim UserID As Integer = Request.QueryString("UserID")
        Dim objVisitor As New visitormaster(UserID)
        Dim Body As String = "<html><head></head><body><table><tr><td colspan='2'>Dear " & contactPerson & ",<br/><br/></td></tr>"
        Body += " <tr><td><h1>Visitor Check-in</h1><hr/><br/></td></tr>"
        Body += " <tr>" & objVisitor.FirstName & " " & objVisitor.SecondName & " wrote</td><tr><td></td></tr>"
        Body += " </tr><tr><td>Hi " & contactPerson & "</td>"
        Body += " </tr><tr><td>" & txtMessage.Text.Trim & "</td></tr><tr><td>Regards, " & objVisitor.FirstName
        Body += " </td></tr><tr><td><img src='" & Session("CapturedImage").ToString() & "'style='width: 320px; height: 240px;'/></td></tr></table></body></html>"
        Dim Subject As String = objVisitor.FirstName & " " & objVisitor.SecondName & " is waiting at the reception"
        Dim ToEmail As String = GetAppSetings("ToEmail")
        sendEmailToUser(ToEmail, Subject, Body)
        Response.Redirect("VisitorReceipt.aspx")
    End Sub

End Class