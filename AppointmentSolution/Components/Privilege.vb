﻿Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports MySql.Data.MySqlClient
''' <summary>
''' Summary description for Privilege
''' </summary>
Public Module Privilege

    Public Function IsSessionValid() As Boolean
        If Not HttpContext.Current.Session("UserName") Is Nothing Then
            Return True
        Else
            HttpContext.Current.Session("lastPage") = HttpContext.Current.Request.Url.ToString
            Return False
        End If
    End Function

    Public Function CheckPrivRequired(Optional ByVal ReportName As String = "") As Boolean
        If ReportName = "" Then ReportName = Privilege.getPageName().ToLower()
        Select Case ReportName.ToLower
            Case "login.aspx"
                Return False
            Case "default.aspx"
                Return False
            Case "dashboard.aspx"
                Return False
            Case "ex-error.aspx"
                Return False
            Case "defaulterror.aspx"
                Return False
            Case Else
                Return True
        End Select
    End Function

    Public Function PagePrive(ByVal PageId As Integer, ByVal PageParentId As Integer) As Boolean
        PagePrive = False
        Dim ps As String = HttpContext.Current.Session("Privilege").ToString
        Dim p1 As String() = ps.Split(";")
        Dim p(p1.Count) As String
        For i As Integer = 0 To p1.Count - 1
            If p1(i).Contains("|") Then
                p(i) = p1(i).Split("|")(0)
            End If
        Next
        If Array.IndexOf(p, PageId.ToString) >= 0 OrElse Array.IndexOf(p, PageParentId.ToString) >= 0 Then
            PagePrive = True 
        End If
        Return PagePrive
    End Function

    Public Function getPageName() As String
        Dim s As String = HttpContext.Current.Request.Path
        Dim sa As String() = s.Split("/")
        If sa.Length > 0 Then
            Return sa(sa.Length - 1)
        Else
            Return "PageNotAvailable"
        End If
    End Function

    Public Function getPageName(ByVal s As String) As String
        Dim sa As String() = s.Split("/")
        If (sa.Length > 0) Then
            Return sa(sa.Length - 1)
        Else
            Return "PageNotAvailable"
        End If
    End Function


    Public Sub ActionPrivilage(SessionString As String, SessionVal As Integer)

        If HttpContext.Current.Session(SessionString) Is Nothing Then
            HttpContext.Current.Session.Add(SessionString, IIf((SessionVal = 1), "true", "false"))
        Else
            HttpContext.Current.Session.Remove(SessionString)
            HttpContext.Current.Session.Add(SessionString, IIf((SessionVal = 1), "true", "false"))
        End If

    End Sub

End Module

