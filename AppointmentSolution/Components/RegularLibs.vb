﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.Mail
Imports System.Data.SqlClient
Imports System.Diagnostics
Public Module RegularLibs

    Public Sub FetchTerminalInfo(Optional ByVal MacOrIPaddress As String = "127.0.0.1")

        'Dim CTinfo As ClientTerminalInfo = New ClientTerminalInfo
        'With CTinfo


        '    '//  .GetTerminalInfo(Request.UserHostAddress) '// LIVE
        '    .GetTerminalInfo(MacOrIPaddress) '//FOR TESTING  "127.0.0.1"


        '    GlobalData("TerminalID") = .ID



        '    GlobalData("MachineID") = .MachineID
        '    GlobalData("BranchID") = .BranchID
        '    GlobalData("DealerID") = .DealerID
        '    GlobalData("MacAddress") = .MacAddress
        '    GlobalData("Status") = .Status
        '    GlobalData("ShowLoginFlag") = .ShowLoginFlag
        '    GlobalData("LastLoginOn") = .LastLoginOn
        '    GlobalData("FileUpdatesFlag") = .FileUpdatesFlag
        '    GlobalData("LastFileUpdatedOn") = .LastFileUpdatedOn
        '    GlobalData("RestartFlag") = .RestartFlag
        '    GlobalData("LastRestartOn") = .LastRestartOn
        '    GlobalData("ServerRequestFreequency") = .ServerRequestFreequency
        '    GlobalData("LastServerRequestOn") = .LastServerRequestOn
        '    GlobalData("TerminalLimit") = .TerminalLimit
        '    GlobalData("Profile") = .Profile
        '    GlobalData("DealerName") = .DealerName
        '    GlobalData("DealerNameAr") = .DealerNameAr
        '    GlobalData("OwnerID") = .OwnerID
        '    GlobalData("CardOwner") = .CardOwner
        '    GlobalData("OwnerName") = .OwnerName
        '    GlobalData("OwnerNameAr") = .OwnerNameAr
        '    GlobalData("DealerLogo") = .DealerLogo
        '    GlobalData("DealerRcptLogo") = .DealerRcptLogo
        '    GlobalData("DealerStatus") = .DealerStatus
        '    GlobalData("BranchStatus") = .BranchStatus
        '    GlobalData("OwnerStatus") = .OwnerStatus
        '    GlobalData("MachineStatus") = .MachineStatus
        '    GlobalData("BranchName") = .BranchName
        '    GlobalData("BranchNameAr") = .BranchNameAr
        '    GlobalData("AreaID") = .AreaID
        '    GlobalData("MachineName") = .MachineName
        '    GlobalData("MachineTypeID") = .MachineTypeID
        '    GlobalData("MSRStatus") = .MSRStatus
        '    GlobalData("WBAStatus") = .WBAStatus
        '    GlobalData("Language") = .LanguageOption




        'End With
        ''Response.Write("<BR>")
        ''Response.Write("<font size='25' color='#FFFFFF'> ERROR : ")
        ''Response.Write(CTinfo.ErrorMessage)
        ''Response.Write("</font>")
        ''Response.End()
    End Sub

    Public Function InsertTempTransaction(ByVal AccountNumber As String, ByVal Amount As Double, ByVal ServiceID As Integer,
                                          ByVal PaymentType As Integer, ByVal TranCharge As Double, ByVal TranFees As Double, ByVal LangUsed As Integer,
                                          ByVal ServiceTypeID As Integer, ByVal CardID As Integer, Optional ByVal MachineType As String = "Kiosk") As String
        Dim errormsg As String = ""
        Dim rt As Integer = 0
        Dim Slno As String = ""
        Dim TransactionTime As DateTime = Now



        ''ID int output, 
        ''TerminalID int, 
        ''ServiceID int, 
        ''AuthID int, 
        ''PaymentType tinyint, 
        ''PaymentStatus tinyint, 
        ''TranStatus tinyint, 
        ''TranAmount decimal(15,3), 
        ''ActualAmountRcvd decimal(15,3), 
        ''TranCharge decimal(15,3), 
        ''TranFees decimal(15,3), 
        ''TranTime datetime, 
        ''TerminalAddress varchar(50), 
        ''Authorization_Code varchar(50), 
        ''EmployeeID int, 
        ''DealerID int, 
        ''LangUsed tinyint, 
        ''TranBoxID int, 
        ''TranSource tinyint, 
        ''ServiceTypeID tinyint, 
        ''CardID int, 
        ''TranSteps tinyint, 
        ''VoucherCode varchar(50), 
        ''VoucherStatus tinyint, 
        ''VoucherTransID int

        'Dim TempDB As New InitTransaction
        'With TempDB
        '    .AccountNumber = AccountNumber
        '    .AuthID = 0
        '    If MachineType = "POS" Then
        '        .PaymentStatus = 1
        '    Else
        '        .PaymentStatus = 0
        '    End If

        '    .PaymentType = PaymentType
        '    .ServiceID = ServiceID
        '    .TerminalAddress = GlobalData("MacAddress").ToString
        '    .TerminalID = GlobalData("TerminalID").ToString
        '    .TranAmount = Amount
        '    .TranCharge = TranCharge
        '    .TranFees = TranFees
        '    .TranStatus = 0
        '    .TranTime = Now
        '    .LangUsed = LangUsed
        '    .ServiceTypeID = ServiceTypeID
        '    .CardID = CardID
        '    .VoucherCode = ""
        '    .TranRmoteID = 0
        '    .TranSource = 0
        '    .SyncStatus = 0
        '    .ServicesCode = GlobalData("ServicesCode").ToString
        '    rt = .Insert
        'End With

        'If rt = 1 Then

        '    Dim Trans As Transaction
        '    If Not GlobalData("Trans") Is Nothing Then
        '        Trans = GlobalData("Trans")
        '    End If

        '    GlobalData("TRANSID") = TempDB.ID
        '    Slno = FormatSerialNumber(Right("00000000" + GlobalData("TerminalID").ToString, 4) + "-" + TempDB.ID.ToString, GlobalData("ServicesCode"))
        '    GlobalData("TRANSCODE") = Slno

        '    Trans.ServicesCode = GlobalData("ServicesCode")
        '    Trans.TransID = TempDB.ID
        '    Trans.TerminalID = GlobalData("TerminalID")
        '    Trans.TransCode = Slno
        '    GlobalData("Trans") = Trans
        'Else
        '    SendInternalEmail("Error", "Temporary Table Insertion failed", "<BR> TRANNO: " & GlobalData("TRANSCODE") & "<BR> MACADDR:" & GlobalData("MacAddress") & _
        '                      "<BR> Service ID: " & GlobalData("ServiceID").ToString & "<BR>Account Number: " & GlobalData("AccountNumber") & "<BR>PAYMODE: CASH <BR> STATUS: Failed @ Cash Nagivate" & _
        '                      "<BR> ErrorMessage: " & TempDB.ErrorMessage, GlobalData("ServicesCode").ToString, GlobalData("TRANSCODE"))
        '    ' RedirectToTransFailpage("Transaction could not proceed...Please Try again.")
        '    Response.Write(TempDB.ErrorMessage)
        'End If
        Return Slno
    End Function

    Public Function maskString(strToMask As String, maskingChar As String, preCharLenthToShow As Integer, postCharLenthToShow As Integer) As String
        Dim rtnValue As String = ""
        Dim prestring As String
        Dim poststring As String
        Dim replacestring As String = ""
        Dim replacelen As Integer = 0

        If strToMask.Length > (preCharLenthToShow + postCharLenthToShow) Then
             
            prestring = strToMask.Substring(0, preCharLenthToShow)
            poststring = strToMask.Substring(strToMask.Length - postCharLenthToShow)
            replacelen = strToMask.Length - (preCharLenthToShow + postCharLenthToShow)
            For i As Integer = 0 To replacelen - 1
                replacestring = replacestring & maskingChar
            Next

            rtnValue = prestring + replacestring + poststring

        Else
            rtnValue = "xxxxxxxxxxxxxxxxxxxxxxxx"

        End If
        Return rtnValue
    End Function

End Module
