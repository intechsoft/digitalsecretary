﻿Imports System.Net
Imports System.Reflection
Imports System.IO
Imports System.Text
Imports System.Web

Public Class RequestLoger
    Private logBuffer As New StringBuilder
    Private Sub AddToLog(ByVal RefId As String, ByVal Message As String)
        Dim L As String
        L = DateTime.Now.ToString("hh:mm:ss.ffff") & " " & RefId.ToString & ": " & Message & Environment.NewLine
        Try
            logBuffer.Append(L)
        Catch ex As Exception
            'do nothing
        End Try
    End Sub

    Private Sub SaveLogFile()
        Dim DPath As String = HttpContext.Current.Server.MapPath("Log\")
        Dim f As FileStream
        Dim FilePath As String
        Try
            If System.IO.Directory.Exists(DPath) = False Then
                System.IO.Directory.CreateDirectory(DPath)
            End If
            FilePath = DPath & "ServiceRequestLog_" & Now.ToString("yyyy-MM-dd") & ".log"
            f = New FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.Write)
            f.Seek(0, SeekOrigin.End)
            Dim data As Byte() = Encoding.Default.GetBytes(logBuffer.ToString)
            f.Write(data, 0, data.Length)
            f.Close()
        Catch ex As Exception
            'do nothing
        End Try
    End Sub
End Class
