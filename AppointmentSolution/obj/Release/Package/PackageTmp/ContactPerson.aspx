﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Appointment.Master" CodeBehind="ContactPerson.aspx.vb" Inherits="AppointmentSolution.ContactPerson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        body {
            font-family: Arial;
        }

        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            /*background-color: #f1f1f1;*/
        }

            /* Style the buttons inside the tab */
            .tab div {
                /*background-color: inherit;*/
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 14px 16px;
                transition: 0.3s;
                font-size: 17px;
            }

                /* Change background color of buttons on hover */
                .tab div:hover {
                    background-color: #ddd;
                }

                /* Create an active/current tablink class */
                .tab div.active {
                    background-color: #ccc;
                }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
        .contactPerson {
        height:40px;
        vertical-align:middle;
        border:0px solid #f0f;
        padding-top:10px;
        cursor:pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contact-us-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="contact-us-label" aria-hidden="true" style="top: 132.4px; position: absolute; display: block;">
        <div class="modal-header" style="border-bottom: 3px solid #fff;">
            <!-- ## heading ## -->
            <h5 style="float: right;">
                <%=Now.ToString("hh:mm:ss tt") %></h5>
            <div style="">
                <h3 id="contact-us-label" style="margin: 0 0 0 0; padding: 0 0 0 0;">Select A Contact Person</h3>
            </div>

        </div>
        <h4></h4>
        <div class="modal-body">
            <!-- ## content ## -->
            <div class="row-fluid col">
                <div id="contact_form" class="span6 offset1">
                    <asp:HiddenField ID="hidContactPerson" runat="server" />
                    <div class="tab ">
                        <div class="tablinks active" onclick="openCity(event, 'HR')">HR</div>
                        <div class="tablinks" onclick="openCity(event, 'IT')">IT</div>
                        <div class="tablinks" onclick="openCity(event, 'Marketing')">Marketing</div>
                        <div class="tablinks" onclick="openCity(event, 'Production')">Production</div>
                        <div class="tablinks" onclick="openCity(event, 'Reception')">Reception</div>
                    </div>

                    <div id="HR" class="tabcontent active">
                        <div class="controls contactPerson" onclick="showMessageBox(this,'HR Contact Person 1');">
                            <asp:Button ID="btnHRContactPerson1" runat="server" Text="HR Contact Person 1" class="control-label" OnClientClick="return false;"
                                style="background-color:transparent; border:none;color:#fff;"/>
                         </div>
                        <div class="controls contactPerson" onclick="showMessageBox('HR Contact Person 2');">
                            <asp:Button ID="btnHRContactPerson2" runat="server" Text="HR Contact Person 2" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('HR Contact Person 3');">
                            <asp:Button ID="btnHRContactPerson3" runat="server" Text="HR Contact Person 3" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('HR Contact Person 4');">
                            <asp:Button ID="btnHRContactPerson4" runat="server" Text="HR Contact Person 4" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('HR Contact Person 5');">
                            <asp:Button ID="btnHRContactPerson5" runat="server" Text="HR Contact Person 5" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                    </div>

                    <div id="IT" class="tabcontent">
                        <div class="controls contactPerson" onclick="showMessageBox('IT Contact Person 1');">
                            <asp:Button ID="btnITContactPerson1" runat="server" Text="IT Contact Person 1" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                         </div>
                        <div class="controls contactPerson" onclick="showMessageBox('IT Contact Person 2');">
                            <asp:Button ID="btnITContactPerson2" runat="server" Text="IT Contact Person 2" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('IT Contact Person 3');">
                            <asp:Button ID="btnITContactPerson3" runat="server" Text="IT Contact Person 3" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('IT Contact Person 4);">
                            <asp:Button ID="btnITContactPerson4" runat="server" Text="IT Contact Person 4" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('IT Contact Person 5');">
                            <asp:Button ID="btnITContactPerson5" runat="server" Text="IT Contact Person 5" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                    </div>

                    <div id="Marketing" class="tabcontent">
                        <div class="controls contactPerson" onclick="showMessageBox('Marketing Contact Person 1');">
                            <asp:Button ID="btnMarketingContactPerson1" runat="server" Text="Marketing Contact Person 1" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                         </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Marketing Contact Person 2');">
                            <asp:Button ID="btnMarketingContactPerson2" runat="server" Text="Marketing Contact Person 2" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Marketing Contact Person 3');">
                            <asp:Button ID="btnMarketingContactPerson3" runat="server" Text="Marketing Contact Person 3" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Marketing Contact Person 4');">
                            <asp:Button ID="btnMarketingContactPerson4" runat="server" Text="Marketing Contact Person 4" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Marketing Contact Person 5');">
                            <asp:Button ID="btnMarketingContactPerson5" runat="server" Text="Marketing Contact Person 5" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                    </div>

                    <div id="Production" class="tabcontent">
                        <div class="controls contactPerson" onclick="showMessageBox('Production Contact Person 1');">
                            <asp:Button ID="btnProductionContactPerson1" runat="server" Text="Production Contact Person 1" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                         </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Production Contact Person 2');">
                            <asp:Button ID="btnProductionContactPerson2" runat="server" Text="Production Contact Person 2" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Production Contact Person 3');">
                            <asp:Button ID="btnProductionContactPerson3" runat="server" Text="Production Contact Person 3" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Production Contact Person 4');">
                            <asp:Button ID="btnProductionContactPerson4" runat="server" Text="Production Contact Person 4" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Production Contact Person 5');">
                            <asp:Button ID="btnProductionContactPerson5" runat="server" Text="Production Contact Person 5" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                    </div>

                    <div id="Reception" class="tabcontent">
                        <div class="controls contactPerson" onclick="showMessageBox('Reception Contact Person 1');">
                            <asp:Button ID="btnReceptionContactPerson1" runat="server" Text="Reception Contact Person 1" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                         </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Reception Contact Person 2');">
                            <asp:Button ID="btnReceptionContactPerson2" runat="server" Text="Reception Contact Person 2" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Reception Contact Person 3');">
                            <asp:Button ID="btnReceptionContactPerson3" runat="server" Text="Reception Contact Person 3" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Reception Contact Person 4');">
                            <asp:Button ID="btnReceptionContactPerson4" runat="server" Text="Reception Contact Person 4" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                        <div class="controls contactPerson" onclick="showMessageBox('Reception Contact Person 5');">
                            <asp:Button ID="btnReceptionContactPerson5" runat="server" Text="Reception Contact Person 5" class="control-label" OnClientClick="return false;" 
                                style="background-color:transparent; border:none;color:#fff;"/>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div id="divMessage" style="display:none;">
                    <div class="controls">
                        <label class="control-label span5 txt" for="txtMessage" style="margin-left:15px;">Enter Your Message</label>
                        <asp:TextBox ID="txtMessage" ClientIDMode="Static" runat="server" rows="10" placeholder="Message" class="span5 txt required" TextMode="MultiLine" style="margin-left:15px;"></asp:TextBox>
                        </div>
                    <div class="controls">
                        <asp:Button ID="btn_Send" runat="server" Text="Next" class="btn btn-submit right" Style="float: right;" OnClientClick=" return validate();"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
            return false;
        }

        document.getElementById("HR").style.display = "block";

        function showMessageBox(selectedDiv, contactPersonName) {
            $("#divMessage").show();
        }

        function validate() {
            var message = $("#txtMessage").val();
            try {
                message = message.trim();
                if (message.length > 0) {
                    return true;
                } else {
                    alert("Message box cannot be left blank!!!");
                    return false;
                }
            } catch (e) { alert(e.message)}
            return false;
        }

    </script>
</asp:Content>
