﻿'------------------------------------------------------------
'created for table [visitormaster]  on 25-Mar-2018
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class visitormaster
    Inherits UneeshHelper
    Public ID As Integer
    Public FirstName As String
    Public SecondName As String
    Public Company As String
    Public Mail As String
    Public MobileNumber As String
    Public VisitedTime As DateTime
    Public Reason As String
    Public Status As Byte

    Public Sub New(Optional ByVal visitormasterID As Integer = 0)
        MyBase.New(visitormasterID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertvisitormaster"
        IUDProcedures.UpdateProcedure = "Updatevisitormaster"
        IUDProcedures.DeleteProcedure = "Deletevisitormaster"
    End Sub
    Protected Overrides Sub getData()
        If ObjId = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from visitormaster where ID=" & ObjId, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            FirstName = IsNullValue(dr("FirstName"), "")
            SecondName = IsNullValue(dr("SecondName"), "")
            Company = IsNullValue(dr("Company"), "")
            Mail = IsNullValue(dr("Mail"), "")
            MobileNumber = IsNullValue(dr("MobileNumber"), "")
            VisitedTime = IsNullValue(dr("VisitedTime"), New Date(0))
            Reason = IsNullValue(dr("Reason"), "")
            Status = IsNullValue(dr("Status"), 0)
        Else
            ObjId = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = {
        New MySqlParameter("@ID", MySqlDbType.Int16),
        New MySqlParameter("@FirstName", MySqlDbType.String),
        New MySqlParameter("@SecondName", MySqlDbType.String),
        New MySqlParameter("@Company", MySqlDbType.String),
        New MySqlParameter("@Mail", MySqlDbType.String),
        New MySqlParameter("@MobileNumber", MySqlDbType.String),
        New MySqlParameter("@VisitedTime", MySqlDbType.DateTime),
        New MySqlParameter("@Reason", MySqlDbType.String),
        New MySqlParameter("@Status", MySqlDbType.Byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(FirstName, DBNull.Value)
        sp_params(2).Value = IsNullValue(SecondName, DBNull.Value)
        sp_params(3).Value = IsNullValue(Company, DBNull.Value)
        sp_params(4).Value = IsNullValue(Mail, DBNull.Value)
        sp_params(5).Value = IsNullValue(MobileNumber, DBNull.Value)
        sp_params(6).Value = dbDate(VisitedTime)
        sp_params(7).Value = IsNullValue(Reason, DBNull.Value)
        sp_params(8).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input

        Return sp_params
    End Function
End Class