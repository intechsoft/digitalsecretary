﻿'Imports System.Web.Mail
Imports System.Net.Mail
Imports System.Web
Imports System.Web.Caching
Imports System.Configuration
Imports System.Web.UI.HtmlControls
Imports System.Text

'Imports System.Net
Public Module ServerSetUtils

    Public eNetZainPostpaidID As String
    Public eNetZainTopupID As String
    Public eNetZainVoucherID As String
    Public eNetVivaPostpaidID As String
    Public eNetVivaTopupID As String
    Public eNetVivaVoucherID As String
    Public eNetOoredooPostpaidID As String
    Public eNetOoredooTopupID As String
    Public eNetOoredooVoucherID As String
    Public eNetJazeeraSportID As String
    Public eNetGammaTVID As String

    Public Structure LogicalCard
        Public ID As Integer
        Public Pin As String
        Public SerialNumber As String
        Public UserName As String
        Public Password As String
        Public Description As String
        Public Period As String
        Public Validity As String
    End Structure
    Public Structure KnetAmountValidator
        Public Result As Boolean
        Public Reson As String
    End Structure
    Public Enum CardStock
        InStock = 1
        OutofStock = 0
    End Enum
    Public Enum ServiceCurrentStatus
        NotinList = 0
        InList = 1
        DisplayOff = 2
        InActive = 3
        DisplayOn = 4
        Active = 5
        Suspend = 6
        Disabled = 7
    End Enum
    Public Enum ProcessStatus
        Failed = 0
        Success = 1
    End Enum

    Public Enum StatusCodes
        SecurityErr = -3
        InvalidUsr = -2
        Success = 1
        Failed = -1
        Service_not_available = 2
        UnAuthorisedRequest = 4
        InvalidChannel = 5
        InvalidDevice = 6
        InvalidCustomer = 7
        ContactUs_not_available = 8
    End Enum

    Public Structure ServiceProcessResult
        Public Status As ProcessStatus
        Public SubscriberBalance As String
        Public ClientRetnVal As String
        Public ErrorMsg As String

        ' Update for remote voucher transactions
        Public RemoteVoucherDetailsID As Integer

    End Structure
    Public Structure SqlExeResponse

        Public SqlExeFlag As Integer
        Public QstrData As String

    End Structure

    Public Structure TransStatus
        Public Flag As Byte
        Public Steps As Integer
        Public Code As Integer
        Public Description As String
    End Structure

    Public Structure SMSResponse
        Public Status As TransStatus
        Public Result As String
    End Structure

    Public Structure RefillCashUAmountResponse
        Public Flag As ProcessStatus
        Public Status As TransStatus
        Public Description As String
        Public AmountInKD As Decimal
    End Structure

    Public Structure RemoteClientBalanceResponse
        Public AmountDue As Decimal
        Public CurrentBalance As Decimal
        Public Status As Integer
        Public StatusDesc As String
    End Structure

    Public Enum ServiceList

        '' '' '' '' '' '' '' '' '' '' '' '' ''///LIVE

        OoredooVoucher = 6
        OoredooPostPaid = 7
        OoredooTopup = 8
        OoredooPostPaideNet = 37
        OoredooTopupeNet = 38

        ZainVoucher = 1
        ZainPostPaid = 2
        ZainTopup = 3
        ZainPostPaideNet = 4
        ZainTopupeNet = 5

        VivaVoucher = 9
        VivaPostPaid = 10
        VivaTopup = 11
        VivaPostPaideNet = 12
        VivaTopupeNet = 13

        Mada = -100
        CashU = 15
        AtmInTopup = 27

        'JazeeraSport = 35 ' BIS in database
        'GammaTV = 41

        JazeeraSport = 34 ' BIS in database
        GammaTV = 35

        ' '' '' '' '' '' '' '' '' '' '' '' '' '' ''//TEST

        'OoredooVoucher = 4
        'OoredooPostPaid = 5
        'OoredooTopup = 6
        'OoredooPostPaideNet = 59
        'OoredooTopupeNet = 58

        'ZainVoucher = 1
        'ZainPostPaid = 2
        'ZainTopup = 3
        'ZainPostPaideNet = 41
        'ZainTopupeNet = 42

        'VivaVoucher = 7
        'VivaPostPaid = 8
        'VivaTopup = 9
        'VivaPostPaideNet = 43
        'VivaTopupeNet = 44

        'Mada = -100
        'CashU = -100
        'AtmInTopup = 53

    End Enum

    Public Enum PaymentGatewayType
        AlAaly = 0
        Debit = 1
        Credit = 2
    End Enum

    Public Enum PaymentMethod
        Cash = 0
        Card = 1
    End Enum

    Public Function UniqueTerminalId() As String
        UniqueTerminalId = Replace(Trim(TerminalIP), ".", "")
    End Function

    Private Function TerminalKey(ByVal param As String) As String
        TerminalKey = UniqueTerminalId() & param
    End Function

    Public Property GlobalData(ByVal Key As String) As Object
        Get
            Return Cache(TerminalKey(Key))
        End Get
        Set(ByVal value As Object)
            If value Is Nothing Then
                Cache.Remove(TerminalKey(Key))
            Else
                Cache(TerminalKey(Key)) = value
            End If
        End Set
    End Property


    Public ReadOnly Property TerminalIP() As String
        Get
            If AccessType = "Kiosk" Then
                Return Request.UserHostAddress
            Else
                Return DealerId()
            End If
        End Get
    End Property

    Public ReadOnly Property AccessType() As String
        Get
            If HttpContext.Current.Application("AccessType") Is Nothing Then
                HttpContext.Current.Application("AccessType") = ConfigurationManager.AppSettings("AccessType")
            End If
            Return HttpContext.Current.Application("AccessType")
        End Get
    End Property

    Private Function DealerId() As String
        If Request.IsAuthenticated Then
            Return HttpContext.Current.User.Identity.Name
        Else
            Response.Redirect("~/login.aspx")
        End If
    End Function

    Public Sub RefreshPagetoDefaultPath(ByRef Page As System.Web.UI.Page, Optional ByVal TimeInSeconds As Integer = 30)
        Dim MetaTag As New HtmlMeta
        MetaTag.HttpEquiv = "refresh"
        MetaTag.Content = TimeInSeconds.ToString & ";url=" & DefaultSitePath & "default.aspx"
        Page.Header.Controls.Add(MetaTag)
    End Sub

    Public Sub RefreshPagetoDefaultPath(ByRef Page As System.Web.UI.Page, ByVal RedirectPage As String, Optional ByVal TimeInSeconds As Integer = 30)
        Dim MetaTag As New HtmlMeta
        MetaTag.HttpEquiv = "refresh"
        MetaTag.Content = TimeInSeconds.ToString & ";url=" & RedirectPage
        Page.Header.Controls.Add(MetaTag)
    End Sub

    Public Function Cardpage(ByVal CardReaderType As String) As String
        Select Case CardReaderType.ToLower
            Case "magatekswipecard.aspx"
                Cardpage = "~/common/MagatekSwipecard.aspx"
            Case "scancard.aspx"
                Cardpage = "~/common/Scancard.aspx"
            Case "ibmswipecard.aspx"
                Cardpage = "~/common/IBMSwipecard.aspx"
            Case "testswipecard.aspx"
                Cardpage = "~/common/TestSwipecard.aspx"
            Case Else
                Cardpage = "~/common/IBMSwipecard.aspx"
        End Select

        Dim s As String = DefaultSitePath
        If ConfigurationManager.AppSettings("ApplySSL") = "True" Then
            s = Replace(s, "http", "https")
            Cardpage = Replace(Cardpage, "~/", s)
        End If
        Cardpage = Replace(Cardpage, "~/", s)
    End Function

    Public Function CashPage(ByVal CashAcceptorType As String) As String
        Select Case CashAcceptorType.ToLower
            Case "tst"
                CashPage = "~/common/Test_ScanCash.aspx"
            Case "itl"
                CashPage = "~/common/ITL_ScanCash.aspx"
            Case "pyr"
                CashPage = "~/common/PYR_ScanCash.aspx"
            Case "jcm"
                CashPage = "~/common/JCM_ScanCash.aspx"
            Case Else
                CashPage = ""
                RedirectToTransFailpage("Cash is not supported")
        End Select
    End Function

    Public Sub RedirectToDefaultErrorPage(ByVal Msg As String)
        GlobalData("LastError") = Msg
        Response.Redirect(DefaultSitePath & "ErrorPage.aspx")
    End Sub

    Public Sub RedirectToDefaultSQLErrorPage(ByVal Msg As String)
        GlobalData("LastError") = Msg
        Response.Redirect(DefaultSitePath & "SqlErrors.aspx")
    End Sub

    Public Sub RedirectToTransFailpage(ByVal Msg As String)
        GlobalData("LastError") = Msg
        Response.Redirect(DefaultSitePath & "TransFail.aspx")
    End Sub

    Public Function DefaultErrorPage(ByVal Msg As String, ByVal ErrorPage As String) As String
        Return ErrorPage & Msg.Replace(vbCrLf, ". ")
    End Function

    Function IsActiveTime(ByVal FromTime As String, ByVal ToTime As String) As Boolean
        Return (Format(Now(), "HH:mm") > FromTime = True And Format(Now(), "HH:mm") < ToTime = True)
    End Function

    Public Function CurrentPath() As String
        With Request.AppRelativeCurrentExecutionFilePath
            Return .Substring(0, .LastIndexOf("/")) & "/"
        End With
    End Function

    Public Function MaskCard(ByVal GatewayName As String, ByVal Cardnumber As String, Optional ByVal MaskChr As String = "X") As String
        If UCase(GatewayName) <> "PAYMATE" AndAlso UCase(GatewayName) <> "CASH" AndAlso UCase(GatewayName) <> "VOUCHER" AndAlso Cardnumber.Length > 8 Then
            Cardnumber = Left(Cardnumber, 4) & StrDup(Cardnumber.Length - 8, MaskChr) & Right(Cardnumber, 4)
        End If
        Return Cardnumber
    End Function

    Public Function EncDecCardNo(ByVal Cardnumber As String, ByVal Action As Integer) As String
        Select Case Action
            Case 0
                Encrypt(Trim(Cardnumber), "AbhiKee11")
            Case 1
                Decrypt(Trim(Cardnumber), "AbhiKee11")
        End Select
        Return Cardnumber
    End Function

    Public Function FormatSerialNumber(ByVal SerialNumber As String, ByVal ShortCode As String) As String
        SerialNumber = MaskZero(SerialNumber, 12)
        SerialNumber = ShortCode & "" & SerialNumber
        Return SerialNumber
    End Function
    Public Sub ClearGlobalData()
        Dim objItem As DictionaryEntry
        For Each objItem In Cache()
            If InStr(Trim(objItem.Key.ToString()), UniqueTerminalId) <> 0 Then
                Cache.Remove(objItem.Key.ToString())
            End If
        Next
    End Sub
    Public Function CacheValues() As String
        Dim strCache As String = ""
        Dim strKey As String
        Dim strValue As String
        Dim objItem As DictionaryEntry
        For Each objItem In Cache()
            If InStr(Trim(objItem.Key.ToString()), UniqueTerminalId) <> 0 Then
                strKey = Replace(objItem.Key.ToString(), UniqueTerminalId, "")
                strValue = Cache(objItem.Key.ToString()).ToString
                Select Case strKey
                    Case "pin"
                        strValue = ""
                    Case "Cardnumber"
                        strValue = MaskCard("CREDIT", strValue)
                End Select
                strCache &= strKey & "=" & strValue & "<br>"
            End If
        Next
        Return strCache
    End Function
    'Public Sub SendMail2Client(ByVal MailFrom As String, ByVal ToMail As String, ByVal CCMail As String, ByVal SubjectMail As String, ByVal BodyMail As String, Password As String)

    '    'Response.Write("SendMail2Client : " & MailFrom & "</br>" & ToMail & "<br>" & CCMail & "<br>" & SubjectMail & "<br>" & BodyMail & "<br>")


    '    'Dim objMail As New MailMessage()
    '    'With objMail
    '    '    .From = MailFrom
    '    '    .Subject = SubjectMail
    '    '    .To = ToMail
    '    '    .Cc = CCMail
    '    '    .Body = BodyMail
    '    '    .BodyEncoding = Encoding.UTF8
    '    '    .BodyFormat = MailFormat.Html
    '    '    .Priority = Net.Mail.MailPriority.Normal
    '    '    Try
    '    '        SmtpMail.Send(objMail)
    '    '    Catch ex As Exception
    '    '        'LogToEvent("Error Sending Email to " & ToMail & " Subject:" & SubjectMail)
    '    '    End Try
    '    'End With



    '    Dim Client As New SmtpClient("smtp.gmail.com", 587)
    '    Client.DeliveryMethod = SmtpDeliveryMethod.Network
    '    Client.UseDefaultCredentials = False
    '    Client.Credentials = New Net.NetworkCredential(MailFrom, Password)
    '    Client.EnableSsl = True

    '    Dim objMail As New System.Net.Mail.MailMessage()
    '    With objMail

    '        .From = New MailAddress(MailFrom)

    '        .Subject = SubjectMail
    '        .To.Add(New MailAddress(ToMail))
    '        .CC.Add(New MailAddress(CCMail))
    '        .Body = BodyMail
    '        .BodyEncoding = Encoding.UTF8

    '        .Priority = Net.Mail.MailPriority.Normal

    '        .IsBodyHtml = True
    '        ' Set the priority of the mail message to normal


    '        Try
    '            'SmtpMail.SmtpServer.Insert(0, "smtp.qualitynet.net")
    '            'SmtpMail.SmtpServer = Client

    '            Client.Send(objMail)
    '        Catch ex As Exception
    '            'LogToEvent("Error Sending Email to " & ToMail & " Subject:" & SubjectMail)
    '            'Response.Write(ex.Message)
    '        End Try
    '    End With



    'End Sub

    Public Sub SendInternalEmail(ByVal Type As String, ByVal OccuredIn As String, ByVal Msg As String, ByVal Client As String, ByVal TransNo As String)

        'Response.Write("SendInternalEmail : " & Type & "</br>" & OccuredIn & "<br>" & Msg & "<br>" & Client & "<br>" & TransNo & "<br>")

        Dim objMail As New MailMessage()
        Dim IPADD As String = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
        Dim Subject, Body As String
        Dim MailTo, MailFrom As String
        Dim MailBcc As String = "paymate@efinance.com.kw"

        Body = "<br>Date/Time : " & Now() & "<br>From : " & OccuredIn & "<br>LocationIP : " & IPADD & "<br>Service ID : " & Client & "<br> Local Trans ID : " & TransNo & vbCrLf & vbCrLf & Msg
        Select Case Type
            Case "Unexpected Error"
                Subject = Client & " Unexpected Error " & TransNo & " On " & Now().ToString
                MailFrom = ConfigurationManager.AppSettings("FromEmail")
                MailTo = ConfigurationManager.AppSettings("ToEmail")
            Case "Error"
                If TransNo = "N/A" Then
                    Subject = Client & " Transaction Error On " & Now().ToString
                Else
                    Subject = Client & " Transaction Fail " & TransNo & " On " & Now().ToString
                End If
                MailFrom = ConfigurationManager.AppSettings("FromEmail")
                MailTo = ConfigurationManager.AppSettings("ToEmail")
            Case "StatusUpdate"
                Subject = Client & " Status update fail " & TransNo & " On " & Now().ToString
                MailFrom = ConfigurationManager.AppSettings("FromEmail")
                MailTo = ConfigurationManager.AppSettings("ToEmail")
            Case "Result"
                Subject = Client & "  Error On " & Now().ToString
                MailFrom = ConfigurationManager.AppSettings("FromEmail")
                MailTo = ConfigurationManager.AppSettings("ToEmail")
                Body = Body & "Transaction No:= " & TransNo

            Case "Warning"
                Subject = Client & " Transaction Warning " & TransNo & " On " & Now().ToString
                MailFrom = ConfigurationManager.AppSettings("FromEmail")
                MailTo = ConfigurationManager.AppSettings("ToEmail")
        End Select


        Dim MailClient As New SmtpClient("smtp.gmail.com", 587)
        MailClient.DeliveryMethod = SmtpDeliveryMethod.Network
        MailClient.UseDefaultCredentials = False
        MailClient.Credentials = New Net.NetworkCredential(ConfigurationManager.AppSettings("FromEmail"), ConfigurationManager.AppSettings("Password"))
        MailClient.EnableSsl = True

        objMail.From = New MailAddress(MailFrom)

        objMail.Subject = Subject
        objMail.To.Add(New MailAddress(MailTo))
        objMail.CC.Add(New MailAddress(ConfigurationManager.AppSettings("CCMail")))
        objMail.Bcc.Add(New MailAddress(ConfigurationManager.AppSettings("MailBcc")))
        objMail.Body = Body
        objMail.BodyEncoding = Encoding.UTF8


        objMail.Priority = Net.Mail.MailPriority.Normal

        objMail.IsBodyHtml = True
        ' Set the priority of the mail message to normal





        Try
            'SmtpMail.SmtpServer.Insert(0, "smtp.qualitynet.net")
            'SmtpMail.SmtpServer = Client

            MailClient.Send(objMail)
        Catch ex As Exception
            'LogToEvent("Error Sending Email to " & ToMail & " Subject:" & SubjectMail)
            'Response.Write(ex.Message)

            'LogToEvent("Error Sending Email to " & ConfigurationManager.AppSettings("errormailID") & " Subject:" & Subject)
        End Try
    End Sub

End Module
