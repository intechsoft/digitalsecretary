﻿Imports System.IO
Imports System.Web.Services
Public Class VisitorRegistor
    Inherits BaseUIPage

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Session("CapturedImage") = Nothing
            clearForm()
        End If
    End Sub

    Private Shared Function ConvertHexToBytes(hex As String) As Byte()
        Dim bytes As Byte() = New Byte(hex.Length / 2 - 1) {}
        Try
            For i As Integer = 0 To hex.Length - 1 Step 2
                bytes(i / 2) = Convert.ToByte(hex.Substring(i, 2), 16)
            Next
        Catch ex As Exception

        End Try
        Return bytes
    End Function

    <WebMethod(EnableSession:=True)>
    Public Shared Function GetCapturedImage() As String
        Dim url As String = HttpContext.Current.Session("CapturedImage").ToString()
        HttpContext.Current.Session("CapturedImage") = Nothing
        Return url
    End Function

    Private Sub btn_Send_Click(sender As Object, e As EventArgs) Handles btn_Send.Click
        If validateForm() Then
            Dim objvisitormaster As New visitormaster
            With objvisitormaster
                .FirstName = txtFirstName.Text
                .SecondName = txtSecondName.Text
                .Company = txtCompany.Text
                .Mail = txtMail.Text
                .MobileNumber = txtMobileNumber.Text
                .VisitedTime = Now
                .Reason = "NIL"
                .Status = 1
                Dim id As Integer = 0
                id = .insertMember()
                If id > 0 Then
                    If Request.InputStream.Length > 0 Then
                        Using reader As New StreamReader(Request.InputStream)
                            Dim hexString As String = Server.UrlEncode(reader.ReadToEnd())
                            'Dim imageName As String = DateTime.Now.ToString("dd-MM-yy hh-mm-ss")
                            Dim imagePath As String = String.Format("~/Captures/{0}.png", id)
                            File.WriteAllBytes(Server.MapPath(imagePath), ConvertHexToBytes(hexString))
                            Session("CapturedImage") = ResolveUrl(imagePath)
                        End Using
                    End If
                    Response.Redirect("ContactPerson.aspx?UserID=" & id)
                Else
                    contact_form_message.Text = .ErrorMsg
                End If
            End With
        End If
    End Sub

    Private Function validateForm() As Boolean
        Dim retVal As Boolean = False
        If txtCompany.Text.Trim.Length > 0 Then
            If txtFirstName.Text.Length > 0 Then
                If txtMail.Text.Length > 0 Then
                    If txtMobileNumber.Text.Length > 0 Then
                        If txtSecondName.Text.Length > 0 Then
                            retVal = True
                        End If
                    End If
                End If
            End If
        End If
        Return retVal
    End Function

    Private Sub clearForm()
        txtCompany.Text = ""
        txtFirstName.Text = ""
        txtMail.Text = ""
        txtMobileNumber.Text = ""
        txtSecondName.Text = ""
    End Sub

End Class