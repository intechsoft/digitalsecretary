﻿
'------------------------------------------------------------
'created for table [xmlservicelog_formobileapp]  on 29-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class xmlservicelog_formobileapp
    Inherits UneeshHelper
    Public ID As Integer
    Public IPAddress As String
    Public MaccID As String
    Public DeviceID As String
    Public MobileNumber As String
    Public CustomerID As Integer
    Public ChannelID As Integer
    Public MethodndParms As String
    Public PageName As String
    Public LogDate As DateTime
    Public Status As Byte
    Public AppTypeId As Byte
    Public Version As String

    Public Sub New(Optional ByVal xmlservicelog_formobileappID As Integer = 0)
        MyBase.New(xmlservicelog_formobileappID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertxmlservicelog_formobileapp"
        IUDProcedures.UpdateProcedure = "Updatexmlservicelog_formobileapp"
        IUDProcedures.DeleteProcedure = "Deletexmlservicelog_formobileapp"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from xmlservicelog_formobileapp where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            IPAddress = IsNullValue(dr("IPAddress"), "")
            MaccID = IsNullValue(dr("MaccID"), "")
            DeviceID = IsNullValue(dr("DeviceID"), "")
            MobileNumber = IsNullValue(dr("MobileNumber"), "")
            CustomerID = IsNullValue(dr("CustomerID"), 0)
            ChannelID = IsNullValue(dr("ChannelID"), 0)
            MethodndParms = IsNullValue(dr("MethodndParms"), "")
            PageName = IsNullValue(dr("PageName"), "")
            LogDate = dr("LogDate")
            Status = dr("Status")
            AppTypeId = IsNullValue(dr("AppTypeId"), 0)
            Version = IsNullValue(dr("Version"), "")
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@IPAddress", MySqlDbType.String, 50), _
        New MySqlParameter("@MaccID", MySqlDbType.String, 50), _
        New MySqlParameter("@DeviceID", MySqlDbType.String, 50), _
        New MySqlParameter("@MobileNumber", MySqlDbType.String, 50), _
        New MySqlParameter("@CustomerID", MySqlDbType.Int16), _
        New MySqlParameter("@ChannelID", MySqlDbType.Int16), _
        New MySqlParameter("@MethodndParms", MySqlDbType.String, 500), _
        New MySqlParameter("@PageName", MySqlDbType.String, 50), _
        New MySqlParameter("@LogDate", MySqlDbType.DateTime), _
        New MySqlParameter("@Status", MySqlDbType.byte), _
        New MySqlParameter("@AppTypeId", MySqlDbType.byte), _
        New MySqlParameter("@Version", MySqlDbType.String, 50)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(IPAddress, DBNull.Value)
        sp_params(2).Value = IsNullValue(MaccID, DBNull.Value)
        sp_params(3).Value = IsNullValue(DeviceID, DBNull.Value)
        sp_params(4).Value = IsNullValue(MobileNumber, DBNull.Value)
        sp_params(5).Value = IsNullValue(CustomerID, 0)
        sp_params(6).Value = IsNullValue(ChannelID, 0)
        sp_params(7).Value = IsNullValue(MethodndParms, DBNull.Value)
        sp_params(8).Value = IsNullValue(PageName, DBNull.Value)
        sp_params(9).Value = LogDate
        sp_params(10).Value = Status
        sp_params(11).Value = IsNullValue(AppTypeId, 0)
        sp_params(12).Value = IsNullValue(Version, DBNull.Value)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input
        sp_params(11).Direction = ParameterDirection.Input
        sp_params(12).Direction = ParameterDirection.Input

        Return sp_params
    End Function
End Class
