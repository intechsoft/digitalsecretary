﻿Imports System.Web.Services.Protocols
Public Class AuthHeader
    Inherits SoapHeader
    'Public TerminalId As Integer
    'Public MacAddress As String
    'Public Language As Integer = 0 '0 English 1 Arabic
    'Public AppTypeID As Integer
    'Public Version As String
    'Public WSDLKey As String

    'Protected Friend ID As Integer
    'Protected Friend MachineID As Integer
    'Protected Friend OwnerId As Integer
    'Protected Friend CardOwner As Integer
    'Protected Friend BranchID As Integer
    'Protected Friend ValidateMacAddress As Integer
    'Protected Friend RealMacAddress As String
    'Protected Friend TerminalLimit As Decimal

End Class

Public Class MobileAuthHeader
    Inherits SoapHeader

    Public CustomerID As Integer
    Public MacAddress As String 'Local Encrypted Key1 which also known as Device data
    Public Language As Integer = 0 '0 English 1 Arabic
    Public AppTypeID As Integer
    Public Version As String
    Public WSDLKey As String 'Local Key Plain Text For Identifieng Version of WSDL
    Public DeviceType As String
    Public DeviceID As Integer
    Public MobileNumber As String 
    Public ChannelID As String
     

End Class

